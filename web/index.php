<?php

// Al comentar estas dos lineas
// el proyecto pasa a produccion (explotacion)
// lo cual quiere decir que no nos va a debuguear y no muestra los errores
// en pantalla
// no sale la barra de depuracion
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
