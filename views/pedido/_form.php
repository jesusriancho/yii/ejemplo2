<?php

use app\models\Cliente;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?php

    $clientes = Cliente::find()->all();

    $listData = ArrayHelper::map($clientes, 'id', 'nombre');

    echo $form->field($model, 'id_cliente')->dropDownList(
        $listData,
        ['prompt' => 'Seleccione un cliente']
    );
    ?>

    <?= $form->field($model, 'id_comercial')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

