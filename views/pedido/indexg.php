<?php

use app\models\Pedido;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pedido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'total',
            'fecha',
            'id_cliente',
            // utilizar una relacion
            // opcion 1
            'cliente.nombre', // coloco el nombre de la relacion (esta en el getter del modelo).nombre del campo a mostrar
            // opcion 2
            [
                'label' => 'Nombre del cliente',
                'value' => 'cliente.nombre'
            ],

            'id_comercial',
            // utilizar una relacion
            'comercial.nombre',
            [
                'label' => 'Datos del comercial',
                'value' => function ($modelo) {
                    return "{$modelo->comercial->nombre} {$modelo->comercial->apellido1} {$modelo->comercial->apellido2}";
                }
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pedido $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>
