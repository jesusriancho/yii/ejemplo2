<div class="card">
        <div class="card-body">
            <h5 class="card-title">Pedido <?= $model->id ?></h5>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Fecha: <?= $model->fecha ?></li>
            <li class="list-group-item">Total: <?= $model->total ?></li>
            <li class="list-group-item">Cliente: <?= $model->id_cliente ?></li>
            <li class="list-group-item">Nombre cliente: <?= $model->cliente->nombre . ' ' . $model->cliente->apellido1 . ' ' . $model->cliente->apellido2 ?></li>
            <li class="list-group-item">Comercial: <?= $model->id_comercial ?></li>
        </ul>
    </div>
